$(document).ready(function (){

	window.addEventListener('load', function(){
		$('.loader__brand').fadeIn(500);
		$('.loader__line').addClass('loading');
		setTimeout( function(){
			$('.loader__brand').fadeOut(600);
			$('.loader').fadeOut(2000);
		}, 2100);
		var iterationDelay = 0;
		//setTimeout( function(){
			$('.cover__content').removeClass('starting').addClass('start');
		//}, 80);
		/*$( '.title div' ).each(function() {
			var $letter = $(this);
			setTimeout( function(){
				$letter.addClass('start');
				console.log('Show Letters');
			}, iterationDelay+=80);
		});*/
	});
	
	var swiper = new Swiper('.swiper-container--global', {
        speed: 600,
        direction: 'vertical',
        mousewheel: {
			releaseOnEdges: true,
		},
        on:{
        	slideChange: function(){
        		var page = swiper.activeIndex + 1;
        		$('.js-data-page').text('0' + page + '.');

        		var targetVideo = window.innerWidth <= 767 ? "mobileVideo" : "desktopVideo";
        		var video = document.getElementById(targetVideo);
        		if(page == 4){
        			video.play();
        		}else{
        			if (!video.paused) {
						video.pause();
					}
        		}
        	}
        }

    });

    $('.js-scroll-down').on('click', function(){
    	swiper.slideNext();
    });

    var swiperCover = new Swiper('.swiper-container--cover', {
        pagination: {
	        el: '.swiper-pagination--cover',
	        clickable: true,
	      },
        speed: 600,
        autoplay: {
		    delay: 5000,
		},
		loop: true
    });

    var swiperMaterial = new Swiper('.swiper-container--material', {
    	pagination: {
	        el: '.swiper-pagination--material',
	        clickable: true,
	      },
        speed: 600,
        autoplay: {
		    delay: 5000,
		},
		loop: true
    });

    var swiperProcess = new Swiper('.swiper-container--process', {
    	pagination: {
	        el: '.swiper-pagination--process',
	        clickable: true,
	      },
        speed: 600,
        autoplay: {
		    delay: 5000,
		},
		loop: true
    });

});